//
// Created by Ben on 27/09/2016.
//

#include "helper.h"

helper::helper()
{

}

helper::~helper()
{

}

void helper::fillEdges(uchar *bg, uchar *dss, uchar *mask, uchar *hsv, unsigned int faceLeft, unsigned int faceTop)//, int column, int row, int cameFromColumn, int cameFromRow)
{
    unsigned int topRow = 0, topColumn = 0;
    int lastLeftRow, lastRightRow;

    if(faceLeft != 0)
    {
        getTopOfHead(bg, mask, faceLeft, faceTop, &topColumn, &topRow);
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev44654>>> %d   %d", topRow, topColumn);
    }
    else
    {
        getFirstObject(bg, mask, &topColumn, &topRow);
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev1234>>> %d   %d", topRow, topColumn);
    }

    try
    {
        if(topColumn > WIDTH)
            topColumn = WIDTH;
        else if( topColumn < 0)
            topColumn = 0;
        if(topRow > HEIGHT)
            topRow = HEIGHT;
        else if(topRow < 0)
            topRow = 0;

        fillShape(bg, dss, mask, hsv, topColumn, topRow, 0, &lastLeftRow, &lastRightRow);
    }
    catch (std::exception e)
    {
//        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev7890>>> %d   %s", topRow, e.what());
    }
//    catch (...)
//    {
////        e.what();
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev7890>>> %d   %d", topRow, topRow);
//    }

}

void helper::getTopOfHead(uchar* bg, uchar* mask, unsigned int faceLeft, unsigned int faceTop, unsigned int *topColumn, unsigned int *topRow)//, int*startCol, int*startRow)
{
    int index2, index;//, column, row;// y = 0, x,
    int c, r;
    c = faceTop;    // = column
    faceLeft *= 0.8f;
    index = (faceLeft * WIDTH*4)+(c * 4);
    index2 = (faceLeft * WIDTH) + c;

    for (r = faceLeft; r < HEIGHT - 2; r++, index2 += WIDTH, index += WIDTH_X_4)//, x++ | x axis
    {
        bg[index] = 0;
        bg[index + 1] = 255;
        bg[index + 2] = 0;

        if(mask[index2] == WHITE_PIXEL)
        {
            break;
        }
        if(mask[index2 + 1] == WHITE_PIXEL)// above one pixel
        {
//            column = c + 1;
            c++;
            break;
        }
        if(mask[index2 - 1] == WHITE_PIXEL)// under one pixel
        {
//            column = c - 1;
            c--;
            break;
        }
        if(mask[index2 + 2] == WHITE_PIXEL)// above two pixel
        {
//            column = c + 2;
            c+=2;
            break;
        }
        if(mask[index2 - 2] == WHITE_PIXEL)// under two pixel
        {
//            column = c - 2;
            c-=2;
            break;
        }
    }
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev44478>2>>//////////////////// %d  %d  %d ///////////////////////////", faceLeft, r, x);

    getToTop(bg, mask, c, r, true, topColumn, topRow);
}

void helper::getFirstObject(uchar* bg, uchar* mask, unsigned int *topColumn, unsigned int *topRow)
{
    bool foundObject(false);
    int index2;

    for (int c = WIDTH - 2, r; c > 2; c--) // y axis
    {
        index2 = c - 1;
        for (r = 1; r < HEIGHT; r++, index2 += WIDTH)// x axis
        {
            if (mask[index2] == WHITE_PIXEL)
            {
                foundObject = true;
                break;
            }
        }

        if (foundObject)
        {
            *topRow = r;
            *topColumn = c;
            break;
        }
    }
}

//void helper::getToTop(uchar* bg, uchar* mask, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topColumn, unsigned int *topRow)
//{
//    int index2, index, doubleWidth = WIDTH + WIDTH;
//
//    int nextIndex, inner = 0;
//    bool notFound(false);
//
//    if(isGoingRight)
//    {
//        while (3 < column && column < WIDTH - 3)
//        {
//            column++;
//            index2 = (WIDTH * row) + column - 1;
//
//            //////////Testing//////////
//            index = (column * 4);
//            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
//            {
//                bg[index] = 255;
//                bg[index + 1] = 0;
//                bg[index + 2] = 0;
//            }
//            //////////End Testing//////////
//
//
//            if (mask[index2] == WHITE_PIXEL) // the next connecting pixel is on the same line as the last found pixel
//                inner = 0;           // nothing to do here
//            else if (index2 + WIDTH - 1 < MAX_SIZE &&
//                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//                column--;
//            }
//            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
//            {
//                inner = 1;
//            }
//            else if (index2 + doubleWidth < MAX_SIZE &&
//                     mask[index2 + doubleWidth] == WHITE_PIXEL) // next 2 pixels
//            {
//                inner = 2;
//            }
//            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
//            {
//                inner = -1;
//            }
//            else if (index2 - doubleWidth > 0 &&
//                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
//            {
//                inner = -2;
//            }
//
//            else if (index2 + doubleWidth - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
//                                                              WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 2;
//                column--;
//            }
//            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (!notFound)// no pixel found near, going to the next column
//            {
//                column--;
//                column--;
//                break;
////            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
////            continue;
//            }
////        else break;// no pixel found near, 2nd run, breaking
//
//            if (inner < 0)
//            {
//                inner *= -1;
//                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
////          inner--;
//                row -= inner;
//            }
//            else if (inner > 0)
//            {
//                while (index2 + (WIDTH * inner) < MAX_SIZE &&
//                                   mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
////            inner--;
//                row += inner;
//            }
////        else
////        {
////            inner = 1;
////            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
////                inner++;
////            if(inner > 1)
////            {
////                row += (inner - 1);
////            }
////            else
////            {
////                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
////                    inner++;
////                if(inner > 1)
////                {
////                    row -= (inner + 1);
////                }
////            }
////        }
//        }
//    }
//    else
//    {
//        while (3 < column && column < WIDTH - 3)
//        {
//            column++;
//            index2 = (WIDTH * row) + column - 1;
//
//            //////////Testing//////////
//            index = (column * 4);
//            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
//            {
//                bg[index] = 255;
//                bg[index + 1] = 0;
//                bg[index + 2] = 0;
//            }
//            //////////End Testing//////////
//
//
////        else break;// no pixel found near, 2nd run, breaking
//            if (mask[index2] == WHITE_PIXEL) // the next connecting pixel is on the same column as the last found pixel
//                inner = 0;           // nothing to do here
//            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
//            {
//                inner = -1;
//            }
//            else if (index2 - WIDTH + 1 > 0 &&
//                     mask[index2 - WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column++;
//            }
//            else if (index2 - WIDTH + 2 > 0 &&
//                      mask[index2 - WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column+=2;
//            }
//            else if (index2 - WIDTH - 1 > 0 &&
//                     mask[index2 - WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = -1;
//                column--;
//            }
//
//            else if (index2 + WIDTH > 0 &&
//                     mask[index2 + WIDTH] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//            }
//            else if (index2 + WIDTH + 1 > 0 &&
//                     mask[index2 + WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//                column++;
//            }
//            else if (index2 + WIDTH + 2 > 0 &&
//                     mask[index2 + WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 2;
//                column++;
//            }
//
//            else
//            {
//                column--;
//                break;
//            }
//
//
//
//
////            else if (index2 - doubleWidth > 0 &&
////                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
////            {
////                inner = -2;
////            }
////
////
////            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
////            {
////                inner = 1;
////            }
////            else if (index2 + WIDTH + WIDTH < MAX_SIZE &&
////                     mask[index2 + WIDTH + WIDTH] == WHITE_PIXEL) // next 2 pixels
////            {
////                inner = 2;
////            }
////            else if (index2 + WIDTH - 1 < MAX_SIZE &&
////                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
////            {
////                inner = 1;
////                column--;
////            }
////            else if (index2 + WIDTH + WIDTH - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
////                                                              WHITE_PIXEL) // below row next column// 2 pixels
////            {
////                inner = 2;
////                column--;
////            }
////            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
////            {
////                inner--;
////                column++;
////            }
////            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
////            {
////                inner--;
////                column++;
////            }
////            else if (!notFound)// no pixel found near, going to the next column
////            {
////                column--;
////                column--;
////                break;
//////            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
//////            continue;
////            }
//
//            if (inner <= 0)
//            {
//                inner *= -1;
//                inner++;
//                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
//                inner--;
////          inner--;
//                row -= inner;
//            }
//            else if (inner > 0)
//            {
//                inner++;
//                while (index2 + (WIDTH * inner) < MAX_SIZE &&
//                                   mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
//                    inner++;
//                inner--;
//                row += inner;
//            }
////        else
////        {
////            inner = 1;
////            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
////                inner++;
////            if(inner > 1)
////            {
////                row += (inner - 1);
////            }
////            else
////            {
////                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
////                    inner++;
////                if(inner > 1)
////                {
////                    row -= (inner + 1);
////                }
////            }
////        }
//        }
//    }
//
//
//
//    *topColumn = column;
//    *topRow = row;
//
//
//    /////////another test
//    index2 = (WIDTH * row) + column ;
////    index = (WIDTH * row * 4) + ((column - 3) * 4 );
////
//
////    int WIDTH = 640, HEIGHT = 480, index, index2;
//    for (int r = 0; r < HEIGHT; r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
//        {
////            if (mask.data[index2] == 255)
////            {
////                bg.data[index] = 255;
////                bg.data[index + 1] = 255;
////                bg.data[index + 2] = 255;
////            }
////            else
////            {
////                bg.data[index] = 0;
////                bg.data[index + 1] = 0;
////                bg.data[index + 2] = 0;
////            }
//
//            // we get the image rotated, and so the face detection is rotated as well
//
//            //  faceRight -> top
//            //  faceTop   -> left
//            //  faceLeft  -> bottom
//            //  faveBottom-> right
//
////            if(faceLeft!=0)
//            {
//                if (c == column)// && r >= faceTop && r <= faceBottom)
//                {
//                    bg[index] = 0;
//                    bg[index + 1] = 0;
//                    bg[index + 2] = 255;
//                }
//
//                if (r == row)// && c >= faceLeft && c <= faceRight)
//                {
//                    bg[index] = 0;
//                    bg[index + 1] = 0;
//                    bg[index + 2] = 255;
//                }
//
////                if (c == faceLeft && r >= faceTop && r <= faceBottom)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
////
////                if (r == faceBottom && c >= faceLeft && c <= faceRight)
////                {
////                    bg.data[index] = 0;
////                    bg.data[index + 1] = 0;
////                    bg.data[index + 2] = 255;
////                }
//            }
//        }
//    }
//    bg[index] = 0;
//    bg[index + 1] = 0;
//    bg[index + 2] = 255;
//
//    bg[index + 4] = 0;
//    bg[index + 1 + 4] = 0;
//    bg[index + 2 + 4 ] = 255;
//
//    bg[index - 4] = 0;
//    bg[index + 1 - 4] = 0;
//    bg[index + 2 - 4 ] = 255;
//
//}

void helper::getToTop(uchar* bg, uchar* mask, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topColumn, unsigned int *topRow)
{
    int index2, index, doubleWidth = WIDTH + WIDTH;

    int nextIndex, inner = 0;
    bool notFound(false);

    if(isGoingRight)
    {
        while (3 < column && column < WIDTH - 3)
        {
            column++;
            index2 = (WIDTH * row) + column - 1;

            //////////Testing//////////
            index = (column * 4);
            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
            {
                bg[index] = 255;
                bg[index + 1] = 0;
                bg[index + 2] = 0;
            }
            //////////End Testing//////////


            if (mask[index2] ==
                WHITE_PIXEL) // the next connecting pixel is on the same line as the last found pixel
                inner = 0;           // nothing to do here

            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
            {
                inner = -1;
            }
            else if (index2 - WIDTH + 1> 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
            {
                inner = -1;
                column++;
            }
            else if (index2 - doubleWidth > 0 &&
                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
            {
                inner = -2;
            }
            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
            {
                inner = 1;
            }
            else if (index2 + doubleWidth < MAX_SIZE &&
                     mask[index2 + doubleWidth] == WHITE_PIXEL) // next 2 pixels
            {
                inner = 2;
            }
            else if (index2 + WIDTH - 1 < MAX_SIZE &&
                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = 1;
                column--;
            }
            else if (index2 + doubleWidth - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
                                                            WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = 2;
                column--;
            }
            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
            {
                inner--;
                column++;
            }
            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
            {
                inner--;
                column++;
            }
            else if (!notFound)// no pixel found near, going to the next column
            {
                column--;
                column--;
                break;
//            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
//            continue;
            }
//        else break;// no pixel found near, 2nd run, breaking

            if (inner < 0)
            {
                inner *= -1;
                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
                    inner++;
//          inner--;
                row -= inner;
            }
            else if (inner > 0)
            {
                while (index2 + (WIDTH * inner) < MAX_SIZE &&
                       mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
                    inner++;
//            inner--;
                row += inner;
            }
//        else
//        {
//            inner = 1;
//            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
//                inner++;
//            if(inner > 1)
//            {
//                row += (inner - 1);
//            }
//            else
//            {
//                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
//                    inner++;
//                if(inner > 1)
//                {
//                    row -= (inner + 1);
//                }
//            }
//        }
        }
    }
    else
    {
        while (3 < column && column < WIDTH - 3)
        {
            column++;
            index2 = (WIDTH * row) + column - 1;

            //////////Testing//////////
            index = (column * 4);
            for (int i = 0; i < row && index < MAX_SIZE - 3; i++, index += WIDTH_X_4)
            {
                bg[index] = 255;
                bg[index + 1] = 0;
                bg[index + 2] = 0;
            }
            //////////End Testing//////////


//        else break;// no pixel found near, 2nd run, breaking
            if (mask[index2] == WHITE_PIXEL) // the next connecting pixel is on the same column as the last found pixel
                inner = 0;           // nothing to do here
            else if (index2 - WIDTH > 0 && mask[index2 - WIDTH] == WHITE_PIXEL) // ex pixel
            {
                inner = -1;
            }
            else if (index2 - WIDTH + 1 > 0 &&
                     mask[index2 - WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = -1;
                column++;
            }
            else if (index2 - WIDTH + 2 > 0 &&
                     mask[index2 - WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = -1;
                column+=2;
            }
            else if (index2 - WIDTH - 1 > 0 &&
                     mask[index2 - WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = -1;
                column--;
            }

            else if (index2 + WIDTH > 0 &&
                     mask[index2 + WIDTH] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = 1;
            }
            else if (index2 + WIDTH + 1 > 0 &&
                     mask[index2 + WIDTH + 1] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = 1;
                column++;
            }
            else if (index2 + WIDTH + 2 > 0 &&
                     mask[index2 + WIDTH + 2] == WHITE_PIXEL) // below row next column// 2 pixels
            {
                inner = 2;
                column++;
            }

            else
            {
                column--;
                break;
            }




//            else if (index2 - doubleWidth > 0 &&
//                     mask[index2 - doubleWidth] == WHITE_PIXEL) // ex 2 pixels
//            {
//                inner = -2;
//            }
//
//
//            else if (index2 + WIDTH < MAX_SIZE && mask[index2 + WIDTH] == WHITE_PIXEL) // next pixel
//            {
//                inner = 1;
//            }
//            else if (index2 + WIDTH + WIDTH < MAX_SIZE &&
//                     mask[index2 + WIDTH + WIDTH] == WHITE_PIXEL) // next 2 pixels
//            {
//                inner = 2;
//            }
//            else if (index2 + WIDTH - 1 < MAX_SIZE &&
//                     mask[index2 + WIDTH - 1] == WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 1;
//                column--;
//            }
//            else if (index2 + WIDTH + WIDTH - 1 < MAX_SIZE && mask[index2 + WIDTH + WIDTH - 1] ==
//                                                              WHITE_PIXEL) // below row next column// 2 pixels
//            {
//                inner = 2;
//                column--;
//            }
//            else if (mask[index2 + 1] == WHITE_PIXEL) // upper column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (mask[index2 + WIDTH + 1] == WHITE_PIXEL) // upper row next column// 2 pixels
//            {
//                inner--;
//                column++;
//            }
//            else if (!notFound)// no pixel found near, going to the next column
//            {
//                column--;
//                column--;
//                break;
////            notFound = true; // todo: need to reset notFound back to false every time we find neighbour pixel
////            continue;
//            }

            if (inner <= 0)
            {
                inner *= -1;
                inner++;
                while (index2 - (WIDTH * inner) > 0 && mask[index2 - (WIDTH * inner)] == WHITE_PIXEL)
                    inner++;
                inner--;
//          inner--;
                row -= inner;
            }
            else if (inner > 0)
            {
                inner++;
                while (index2 + (WIDTH * inner) < MAX_SIZE &&
                       mask[index2 + (WIDTH * inner)] == WHITE_PIXEL)
                    inner++;
                inner--;
                row += inner;
            }
//        else
//        {
//            inner = 1;
//            while(nextIndex = index2 + (WIDTH * inner) < MAX_SIZE && mask[nextIndex] == WHITE_PIXEL)
//                inner++;
//            if(inner > 1)
//            {
//                row += (inner - 1);
//            }
//            else
//            {
//                while(nextIndex = index2 - (WIDTH * inner) > 0 && mask[nextIndex] == WHITE_PIXEL)
//                    inner++;
//                if(inner > 1)
//                {
//                    row -= (inner + 1);
//                }
//            }
//        }
        }
    }



    *topColumn = column;
    *topRow = row;


    /////////another test
    index2 = (WIDTH * row) + column ;
//    index = (WIDTH * row * 4) + ((column - 3) * 4 );
//

//    int WIDTH = 640, HEIGHT = 480, index, index2;
    for (int r = 0; r < HEIGHT; r++)
    {
        index = WIDTH * r * 4;
        index2 = WIDTH * r;
        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
        {
//            if (mask.data[index2] == 255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//            else
//            {
//                bg.data[index] = 0;
//                bg.data[index + 1] = 0;
//                bg.data[index + 2] = 0;
//            }

            // we get the image rotated, and so the face detection is rotated as well

            //  faceRight -> top
            //  faceTop   -> left
            //  faceLeft  -> bottom
            //  faveBottom-> right

//            if(faceLeft!=0)
            {
                if (c == column)// && r >= faceTop && r <= faceBottom)
                {
                    bg[index] = 0;
                    bg[index + 1] = 0;
                    bg[index + 2] = 255;
                }

                if (r == row)// && c >= faceLeft && c <= faceRight)
                {
                    bg[index] = 0;
                    bg[index + 1] = 0;
                    bg[index + 2] = 255;
                }

//                if (c == faceLeft && r >= faceTop && r <= faceBottom)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
//
//                if (r == faceBottom && c >= faceLeft && c <= faceRight)
//                {
//                    bg.data[index] = 0;
//                    bg.data[index + 1] = 0;
//                    bg.data[index + 2] = 255;
//                }
            }
        }
    }
    bg[index] = 0;
    bg[index + 1] = 0;
    bg[index + 2] = 255;

    bg[index + 4] = 0;
    bg[index + 1 + 4] = 0;
    bg[index + 2 + 4 ] = 255;

    bg[index - 4] = 0;
    bg[index + 1 - 4] = 0;
    bg[index + 2 - 4 ] = 255;

}


void helper::fillShape(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, unsigned int topCol, unsigned int topRow, unsigned int lastCol
        , int *lastLeftRow, int *lastRightRow)//, unsigned int faceBottomCol)
{
    int indexLeft, indexRight,
            columnLeft = topCol, columnRight = topCol,
            leftSideRow = topRow, rightSideRow = topRow,
            inner, index, nextIndex, hsvIndex;

    int lastLeftLoopRow = topRow, lastLeftLoopCol = topCol;
    int lastRightLoopRow, lastRightLoopCol;

    bool foundEdge;
    int distance = 0, c = topCol;
    bool isLeftGoingLeft(false), isRightGoingRight(false);

    bool isLeftGoingUp(false);

    unsigned int resTopCol, resTopRow;
    int lastLeftSideRow, lastRightSideRow;

//    (sizeof(a)/sizeof(*a)) <<


    //testing
    count = 0;

//            __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", sizeof(mask), sizeof(hsv) / sizeof(*(hsv)));
//
//    int w;
//    int WIDTH = 640, HEIGHT =  480, index, index2;

//    int index2;
//    for (int r = 0; r < HEIGHT * 3; r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for (int c = 0; c < WIDTH * 3; c++, index += 4, index2++)
//        {
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa443>>> %d   %d", index2, r);
//            if(hsv[index2] == 9)
//                ;
//            if(hsv[index2+1] == 9)
//                ;
//            if(hsv[index2+2] == 9)
//                ;
//            if(hsv[index2+3] == 9)
//                ;
//        }
//    }


//    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa>>> %d   %d", *lastLeftRow, lastCol);


    indexLeft = (leftSideRow * WIDTH) + columnLeft;
    inner = 1;
    while( (indexLeft - (WIDTH * inner) > 0 &&
            mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL )
           ||
           ( indexLeft - (WIDTH * inner) - 1 > 0
             && mask[indexLeft - (WIDTH * inner) - 1] == WHITE_PIXEL )
           ||
           ( indexLeft - (WIDTH * inner) + 1 > 0
             && mask[indexLeft - (WIDTH * inner) + 1] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) - 2 > 0
//             && mask[indexLeft - (WIDTH * inner) - 2] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) + 2 > 0
//             && mask[indexLeft - (WIDTH * inner) + 2] == WHITE_PIXEL )

            )
    {
        inner++;
    }
    inner--;
    leftSideRow -= inner;

    indexRight = (rightSideRow * WIDTH) + columnRight;
    inner = 1;
    while(( indexRight + (WIDTH * inner) < MAX_SIZE &&
            mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL )
          ||
          (indexRight + (WIDTH * inner) - 1 < MAX_SIZE &&
           mask[indexRight + (WIDTH * inner) - 1] == WHITE_PIXEL )
          ||
          (indexRight + (WIDTH * inner) + 1 < MAX_SIZE &&
           mask[indexRight + (WIDTH * inner) + 1] == WHITE_PIXEL )
//
//          || (indexRight + (WIDTH * inner) - 2 < MAX_SIZE &&
//              mask[indexRight + (WIDTH * inner) - 2] == WHITE_PIXEL )

//               || (indexRight + (WIDTH * inner) + 2 < MAX_SIZE &&
//                   mask[indexRight + (WIDTH * inner) + 2] == WHITE_PIXEL )
            )
    {
        inner++;
    }
    inner--;
      rightSideRow += inner;

    lastLeftLoopCol = columnLeft;
    lastLeftLoopRow = leftSideRow;

    lastRightLoopCol = columnRight;
    lastRightLoopRow = rightSideRow;

    if(c >= WIDTH - 3)
        return;

    while (c > 3 && c != lastCol)
    {

        indexLeft = (leftSideRow * WIDTH) + columnLeft;
        indexRight = (rightSideRow * WIDTH) + columnRight;



        ///////////////////left side/////////////////////

        inner = 0;
        hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);

        while (indexLeft - (WIDTH * inner) > 0
                           &&
                    (mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
                            &&
                            (
                               (inner == 0
                                &&
                                  hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE) > 0
                               && hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE) < MAX_SIZE_X_3
                               && isNotInRange(hsv[hsvIndex - ( WIDTH_X_3 * DISTANCE_FROM_LINE)], hsv[hsvIndex + ( WIDTH_X_3 * DISTANCE_FROM_LINE)])
                               )
                             ||
                               (
                                       hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) > 0
                                   && hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner)) < MAX_SIZE_X_3
                                   && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner))] , hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * (DISTANCE_FROM_LINE_HALF + inner))])
                               )
                            )
                    )
              )
        {
            inner++;
        }

        if (inner == 0)
        {
            foundEdge = false;
            if(!foundEdge)
            {
                inner = 0;
                do
                {
                    inner++;
                    nextIndex = indexLeft - (WIDTH * inner);

                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);

                    if (nextIndex > 0
                        &&
                        (
                                (mask[nextIndex] == WHITE_PIXEL
                                && hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                                && hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                                && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                                hsv[hsvIndex - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])

//                                 && (isInRange (hsv[ dssIndex - WIDTH - WIDTH - WIDTH]
//                                        , hsv[ dssIndex + WIDTH + WIDTH + WIDTH ])
//                                    ||
//                                     isInRange(hsv[ (leftSideRow * WIDTH_X_4) + (columnLeft*4) + (WIDTH_X_4 * inner) - 3]
//                                        ,hsv[ (leftSideRow * WIDTH_X_4) + (columnLeft*4) + (WIDTH_X_4 * inner) + 3])
//                                    )
                                )
//                         ||
//                                (mask[nextIndex - 1] == WHITE_PIXEL
//                                 && dssIndex - 4 + DISTANCE_FROM_LINE_HALF_X_4 - (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF) > 0
//                                 && dssIndex - 4 - DISTANCE_FROM_LINE_HALF_X_4 + (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_4
//                                 && isNotInRange(hsv[dssIndex - 4 + DISTANCE_FROM_LINE_HALF_X_4 - (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF)],
//                                                 hsv[dssIndex - 4 - DISTANCE_FROM_LINE_HALF_X_4 + (WIDTH_X_4 * DISTANCE_FROM_LINE_HALF)])
//                                )
                         ||
                                (mask[nextIndex + 1] == WHITE_PIXEL
                                 && hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                                 && hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                                 && isNotInRange(hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                                 hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                                )
//                      || mask[nextIndex - 2] == WHITE_PIXEL
//                      || mask[nextIndex + 2] == WHITE_PIXEL
                    )
                            )
                    {
                        foundEdge = true;
                        break;
                    }
                } while (inner < MAX_DISTANCE); // same
            }

            if(!foundEdge)
            {
                inner = 0;
                do
                {
                    inner++;
                    nextIndex = indexLeft - (WIDTH * inner);
                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);

                    if ( nextIndex > 0 && mask[nextIndex + 2] == WHITE_PIXEL
                        && hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                        && hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                        && isNotInRange(hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                        hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                        )
                    {
//                        if(nextIndex - WIDTH > 0 &&
//                            (//mask[nextIndex + 3] == WHITE_PIXEL
////                            ||
//                              (mask[nextIndex - WIDTH] == WHITE_PIXEL
//                               && ( nextIndex - WIDTH - WIDTH > 0
//                                    && mask[nextIndex - WIDTH - WIDTH + 1] == WHITE_PIXEL)
//                             )
//                            ||
//                                    (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
//                                     && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
//                                            (   mask[nextIndex - WIDTH - WIDTH + 3] < MAX_SIZE
//                                             && mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL )
//                                        )
//                                    )
//                            //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
//                            )
//                          )
//                        if(nextIndex - WIDTH > 0 &&
//                           (//mask[nextIndex + 3] == WHITE_PIXEL
////                            ||
//                                (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
//                                 && ( //nextIndex - WIDTH - WIDTH + 3< MAX_DISTANCE &&
//                                        mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL)
//                                )
//                                ||
//                                (//nextIndex + WIDTH + 3 < MAX_SIZE &&
//                                        mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
//                                        && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
//                                                (nextIndex - WIDTH - WIDTH > 0
//                                                 &&    mask[nextIndex - WIDTH - WIDTH + 4] == WHITE_PIXEL
//                                                        //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
//                                                )
//                                        )
//                                )
//                                //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
//                        )
//                                )
//                                inner = 0;
                        foundEdge = true;
                        break;
                    }
                } while (inner < MAX_DISTANCE); // above +2
            }

            if(!foundEdge)
            {
                inner = 0;
                do
                {
                    inner++;
                    nextIndex = indexLeft - (WIDTH * inner);
                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) - (WIDTH_X_3 * inner);
                    if (
                        ( nextIndex > 0 && mask[nextIndex - 2] == WHITE_PIXEL
                        && hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                        && hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                        && isNotInRange(hsv[hsvIndex - 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                        hsv[hsvIndex - 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                        )

                        ||
                        (mask[nextIndex - 1] == WHITE_PIXEL
                         && hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                         && hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                         && isNotInRange(hsv[hsvIndex - 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                         hsv[hsvIndex - 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                        )

                        )


                    {
                        foundEdge = true;
                        inner = 1;
                        break;
                    }
                } while (inner < MAX_DISTANCE); // below -2
            }



            /////////////////////going to the other direction
            if (!foundEdge)
            {
                distance = 0;
                foundEdge = false;
                inner = 0;
                do
                {
                    inner++;
                    //dss index
                    //going right
                    nextIndex = indexLeft + (WIDTH * inner);
                    hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
                    if ( nextIndex < MAX_SIZE
                         &&
                         (
                             (mask[nextIndex] == WHITE_PIXEL && (
                                        hsvIndex + DISTANCE_FROM_LINE_X_3 < MAX_SIZE_X_3
//                                        && dssIndex - DISTANCE_FROM_LINE_X_3 < MAX_SIZE_X_3
                                        && isNotInRange(hsv[hsvIndex + DISTANCE_FROM_LINE_X_3],
                                                        hsv[hsvIndex - DISTANCE_FROM_LINE_X_3])
                                )
                             )
                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
                          ||( mask[nextIndex + 1] == WHITE_PIXEL &&
                                    (
                                            hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                                        && hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                                        && isNotInRange(hsv[hsvIndex + 3 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                                        hsv[hsvIndex + 3 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                                    )
                           )
                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
                          ||( mask[nextIndex + 2] == WHITE_PIXEL &&
                                    (
                                            hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                                        && hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                                        && isNotInRange(hsv[hsvIndex + 6 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                                        hsv[hsvIndex + 6 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                                    )
                            )
                        )
                    )
                    {
                        foundEdge = true;
                        break;
                    }
                } while (distance++ < MAX_DISTANCE);

                if (!foundEdge)
                    inner = 0;
                else inner *= -1;
            }
        }

        ////////////// handling edges

        if (inner > 0) //going left
        {
            inner++;
            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
            //dssindex - distance*4 - (width*inner)
            //dssindex + distance*4 - (width*inner)

            while (indexLeft - (WIDTH * inner) > 0
                                && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL
                                && hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner) > 0
//                                && dssIndex + DISTANCE_FROM_LINE_X_4 - (WIDTH_X_4 * inner) < MAX_SIZE_X_4
                                && isNotInRange(hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner)] ,
                                                hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 - (WIDTH_X_3 * inner)])
                   )
            {
                inner++;
            }
            inner--;
            leftSideRow -= inner;
            isLeftGoingLeft = true;
        }
        else if(inner < 0) //going right
        {
            inner *= -1;
            inner++;
            hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3);
            //dssindex - distance*4 + (width*inner)
            //dssindex + distance*4 + (width*inner)
            while ( indexLeft + (WIDTH * inner) < MAX_SIZE
                            && mask[indexLeft + (WIDTH * inner)] == WHITE_PIXEL
//                            && dssIndex - DISTANCE_FROM_LINE_X_4 + (WIDTH_X_4 * inner) > 0
                            && hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner) < MAX_SIZE_X_3
                            && isNotInRange(hsv[hsvIndex - DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner)] ,
                                   hsv[hsvIndex + DISTANCE_FROM_LINE_X_3 + (WIDTH_X_3 * inner)])
                   )
            {
                inner++;
            }
            inner--;
            leftSideRow += inner;
            isLeftGoingLeft = false;
        }
        else if(mask[indexLeft] != WHITE_PIXEL && false)
        {
            unsigned int selForTopCol = 0, selForTopRow = 0;
            while(inner < MAX_DISTANCE)
            {
                //dss index like case when inner=0
                inner++;
                nextIndex = indexLeft + (inner * WIDTH);
                hsvIndex = (leftSideRow * WIDTH_X_3) + (columnLeft * 3) + (WIDTH_X_3 * inner);
                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL && (
                            hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                            && hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                            && isNotInRange(hsv[hsvIndex + 9 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                            hsv[hsvIndex + 9 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                   ))
                {
                    selForTopCol = c + 3;
                    selForTopRow = leftSideRow + inner;
                    break;
                }
                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL && (
                        hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                        && hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                        && isNotInRange(hsv[hsvIndex + 12 + DISTANCE_FROM_LINE_HALF_X_3 - ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                        hsv[hsvIndex + 12 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                   ))
                {
                    selForTopCol = c + 4;
                    selForTopRow = leftSideRow + inner;
                    break;
                }
                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL && (
                         hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) > 0
                         && hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + ( WIDTH_X_3 * DISTANCE_FROM_LINE_HALF) < MAX_SIZE_X_3
                         && isNotInRange(hsv[hsvIndex + 15 + DISTANCE_FROM_LINE_HALF_X_3 - (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)],
                                         hsv[hsvIndex + 15 - DISTANCE_FROM_LINE_HALF_X_3 + (WIDTH_X_3 * DISTANCE_FROM_LINE_HALF)])
                   ))
                {
                    selForTopCol = c + 5;
                    selForTopRow = leftSideRow + inner;
                    break;
                }
            }

            if(selForTopCol != 0)
            {
                lastLeftSideRow = leftSideRow;
                lastRightSideRow = leftSideRow;
                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",leftSideRow, leftSideRow);

                fillShape(bg, dss, mask, hsv, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);

//                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);

                leftSideRow = lastLeftSideRow - 1;
            }
        }


        if (leftSideRow < 0)
            leftSideRow = 0;

//        __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", leftSideRow, rightSideRow);


        ///////////////////right side II/////////////////////

        inner = 0;
        //add dss index
        //check for inner = 0 and inner != 0
        while (indexRight + (WIDTH * inner) < MAX_SIZE
                           && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
        {
            inner++;
        }

        if (inner == 0)
        {
            distance = 0;
            foundEdge = false;
            inner = 0;
            do
            {
                inner++;
                nextIndex = indexRight + (WIDTH * inner);
                // check for 0 , 1, -1
                if ( nextIndex < MAX_SIZE
                     &&
                     (mask[nextIndex] == WHITE_PIXEL
                      || mask[nextIndex - 1] == WHITE_PIXEL
                      || mask[nextIndex + 1] == WHITE_PIXEL
//                      || mask[nextIndex - 2] == WHITE_PIXEL
//                      || mask[nextIndex + 2] == WHITE_PIXEL
                )
                        )
                {
                    foundEdge = true;
                    break;
                }
            } while (distance++ < MAX_DISTANCE); // same

            if(!foundEdge)
            {
                inner = 0;
                do
                {
                    inner++;
                    nextIndex = indexRight + (WIDTH * inner);
                    //+2
                    if ( nextIndex < MAX_SIZE && mask[nextIndex + 2] == WHITE_PIXEL )
                    {
//                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
//                           (mask[nextIndex + 3] == WHITE_PIXEL
//                            ||mask[nextIndex + WIDTH] == WHITE_PIXEL
//                            ||mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
//                            || mask[nextIndex + WIDTH + 3] == WHITE_PIXEL)
//                                )
//                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
//                           (//mask[nextIndex + 3] == WHITE_PIXEL
////                            ||
//                            (mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
//                               && ( nextIndex + WIDTH + WIDTH + 3< MAX_DISTANCE
//                                    && mask[nextIndex + WIDTH + WIDTH + 3] == WHITE_PIXEL)
//                            )
//                            ||
//                            (//nextIndex + WIDTH + 3 < MAX_SIZE &&
//                             mask[nextIndex + WIDTH + 3] == WHITE_PIXEL
//                             && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
//                                     (nextIndex + WIDTH + WIDTH + 4 < MAX_SIZE
//                                      &&    mask[nextIndex + WIDTH + WIDTH + 4] == WHITE_PIXEL
//                                     //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
//                                     )
//                             )
//                            )
//                                   //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
//                           )
//                                )
//                            inner = 0;
                        foundEdge = true;
                        break;
                    }
                } while (inner < MAX_DISTANCE); // above +2
            }

            if(!foundEdge)
            {
                inner = 0;
                do
                {
                    inner++;
                    //-2
                    nextIndex = indexRight + (WIDTH * inner);
                    if ( nextIndex < MAX_SIZE && mask[nextIndex - 2] == WHITE_PIXEL )
                    {
                        foundEdge = true;
                        inner = 1;
                        break;
                    }
                } while (inner < MAX_DISTANCE); // below -2
            }

            /////////////////////going to the other direction
            if (!foundEdge)
            {
                distance = 0;
                foundEdge = false;
                inner = 0;
                do
                {
                    inner++;
                    //dss index
                    //going left
                    nextIndex = indexRight - (WIDTH * inner);
                    if ( nextIndex > 0
                         &&
                         (mask[nextIndex] == WHITE_PIXEL
                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
                          || mask[nextIndex + 1] == WHITE_PIXEL
                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
                          || mask[nextIndex + 2] == WHITE_PIXEL
                         )
                            )
                    {
                        foundEdge = true;
                        break;
                    }
                } while (distance++ < MAX_DISTANCE);

                if (!foundEdge)
                    inner = 0;
                else inner *= -1;
            }
        }

        ////////////// handling edges

        if (inner > 0) //going right
        {
            inner++;
            //create dss index
            //going right
            // dssindex +distance*4 + (width__x_4 *inner)
            // dssindex -distance*4 + (width__x_4 *inner)

            while (indexRight + (WIDTH * inner) > 0
                               && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
            {
                inner++;
            }
            inner--;
            rightSideRow += inner;
            isRightGoingRight = true;
        }
        else if(inner < 0) //going  left
        {
            // dssindex +distance*4 - (width__x_4 *inner)
            // dssindex -distance*4 - (width__x_4 *inner)
            
            inner *= -1;
            inner++;
            while (indexRight - (WIDTH * inner) > 0
                               && mask[indexRight - (WIDTH * inner)] == WHITE_PIXEL)
            {
                inner++;
            }
            inner--;
            rightSideRow -= inner;
            isRightGoingRight = false;
        }
        else if(mask[indexRight] != WHITE_PIXEL)
        {
            unsigned int selForTopCol = 0, selForTopRow = 0;
            while(inner < 5)
            {
                inner++;
                // dss - like inner = 0
                nextIndex = indexRight - (inner * WIDTH);
                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
                {
                    selForTopCol = c + 3;
                    selForTopRow = rightSideRow + inner;
                    break;
                }
                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
                {
                    selForTopCol = c + 4;
                    selForTopRow = rightSideRow + inner;
                    break;
                }
                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
                {
                    selForTopCol = c + 5;
                    selForTopRow = rightSideRow + inner;
                    break;
                }
            }

            if(selForTopCol != 0 && false)
            {
                unsigned int resTopCol = 0, resTopRow = 0;

                lastLeftSideRow = leftSideRow;
                lastRightSideRow = rightSideRow;

                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);

                fillShape(bg, dss, mask, hsv, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
                rightSideRow = lastRightSideRow + 1;
            }
        }

        if (rightSideRow > HEIGHT)
            rightSideRow = HEIGHT;

        ///////////////////process edges/////////////////////

        if (leftSideRow < rightSideRow)
        {
            *lastLeftRow = leftSideRow;
            *lastRightRow = rightSideRow;

            index = ((leftSideRow * WIDTH) + c) * 4;
            for (inner = leftSideRow; inner < rightSideRow; inner++, index += WIDTH_X_4)
            {
                bg[index] = dss[index];
                bg[index + 1] = dss[index + 1];
                bg[index + 2] = dss[index + 2];
            }
        }

        lastLeftLoopCol = columnLeft;
        lastLeftLoopRow = leftSideRow;

        c--;
        columnLeft--;
        columnRight--;
        count++;
    }

}


bool helper::isNotInRange(uchar arg1, uchar arg2)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "%d called top> first> %d    %d   %d", count, (arg2 + HUE_THRESHOLD)
//            , arg2 - HUE_THRESHOLD
//            , (arg2 + HUE_THRESHOLD < arg1 || arg1 < arg2 - HUE_THRESHOLD));

    return (arg2 + HUE_THRESHOLD < arg1 || arg1 < arg2 - HUE_THRESHOLD);
}

//void helper::fillShape(uchar* bg, uchar* dss, uchar* mask, unsigned int topCol, unsigned int topRow, unsigned int lastCol
//        , int *lastLeftRow, int *lastRightRow)//, unsigned int faceBottomCol)
//{
//    int indexLeft, indexRight,
//            columnLeft = topCol, columnRight = topCol,
//            leftSideRow = topRow, rightSideRow = topRow,
//            inner, index, nextIndex;
//
//    bool foundEdge;
//    int distance = 0, c = topCol;
//    bool isLeftGoingLeft(false), isRightGoingRight(false);
//
//    bool isLeftGoingUp(false);
//
//    unsigned int resTopCol, resTopRow;
//    int lastLeftSideRow, lastRightSideRow;
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa>>> %d   %d", *lastLeftRow, lastCol);
//
//    indexLeft = (leftSideRow * WIDTH) + columnLeft;
//    inner = 1;
//    while( (indexLeft - (WIDTH * inner) > 0 &&
//            mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) - 1 > 0
//             && mask[indexLeft - (WIDTH * inner) - 1] == WHITE_PIXEL )
//           ||
//           ( indexLeft - (WIDTH * inner) + 1 > 0
//             && mask[indexLeft - (WIDTH * inner) + 1] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) - 2 > 0
////             && mask[indexLeft - (WIDTH * inner) - 2] == WHITE_PIXEL )
////           ||
////           ( indexLeft - (WIDTH * inner) + 2 > 0
////             && mask[indexLeft - (WIDTH * inner) + 2] == WHITE_PIXEL )
//
//            )
//    {
//        inner++;
//    }
//    inner--;
//    leftSideRow -= inner;
//
//    indexRight = (rightSideRow * WIDTH) + columnRight;
//    inner = 1;
//    while(( indexRight + (WIDTH * inner) < MAX_SIZE &&
//            mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) - 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) - 1] == WHITE_PIXEL )
//          ||
//          (indexRight + (WIDTH * inner) + 1 < MAX_SIZE &&
//           mask[indexRight + (WIDTH * inner) + 1] == WHITE_PIXEL )
////
////          || (indexRight + (WIDTH * inner) - 2 < MAX_SIZE &&
////              mask[indexRight + (WIDTH * inner) - 2] == WHITE_PIXEL )
//
////               || (indexRight + (WIDTH * inner) + 2 < MAX_SIZE &&
////                   mask[indexRight + (WIDTH * inner) + 2] == WHITE_PIXEL )
//            )
//    {
//        inner++;
//    }
//    inner--;
//    rightSideRow += inner;
//
//    while (c > 3 && c != lastCol && c < WIDTH - 3)
//    {
//
//        indexLeft = (leftSideRow * WIDTH) + columnLeft;
//        indexRight = (rightSideRow * WIDTH) + columnRight;
//
//
//        ///////////////////left side/////////////////////
//
//        inner = 0;
//        while (indexLeft - (WIDTH * inner) > 0
//               && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL)
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//
//            foundEdge = false;
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if (nextIndex > 0
//                        &&
//                        (mask[nextIndex] == WHITE_PIXEL
//                         || mask[nextIndex - 1] == WHITE_PIXEL
//                         || mask[nextIndex + 1] == WHITE_PIXEL
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                    )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // same
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if ( nextIndex > 0 && mask[nextIndex + 2] == WHITE_PIXEL )
//                    {
////                        if(nextIndex - WIDTH > 0 &&
////                            (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                              (mask[nextIndex - WIDTH] == WHITE_PIXEL
////                               && ( nextIndex - WIDTH - WIDTH > 0
////                                    && mask[nextIndex - WIDTH - WIDTH + 1] == WHITE_PIXEL)
////                             )
////                            ||
////                                    (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                     && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                            (   mask[nextIndex - WIDTH - WIDTH + 3] < MAX_SIZE
////                                             && mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL )
////                                        )
////                                    )
////                            //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                            )
////                          )
////                        if(nextIndex - WIDTH > 0 &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                                (mask[nextIndex - WIDTH + 2] == WHITE_PIXEL
////                                 && ( //nextIndex - WIDTH - WIDTH + 3< MAX_DISTANCE &&
////                                        mask[nextIndex - WIDTH - WIDTH + 3] == WHITE_PIXEL)
////                                )
////                                ||
////                                (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                                        mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                                        && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                                (nextIndex - WIDTH - WIDTH > 0
////                                                 &&    mask[nextIndex - WIDTH - WIDTH + 4] == WHITE_PIXEL
////                                                        //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                                )
////                                        )
////                                )
////                                //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                        )
////                                )
////                                inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft - (WIDTH * inner);
//                    if ( nextIndex > 0 && mask[nextIndex - 2] == WHITE_PIXEL )
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexLeft + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE
//                         &&
//                         (mask[nextIndex] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                          || mask[nextIndex + 1] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                          || mask[nextIndex + 2] == WHITE_PIXEL
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0)
//        {
//            inner++;
//            while (indexLeft - (WIDTH * inner) > 0
//                   && mask[indexLeft - (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow -= inner;
//            isLeftGoingLeft = true;
//        }
//        else if(inner < 0)
//        {
//            inner *= -1;
//            inner++;
//            while (indexLeft + (WIDTH * inner) < MAX_SIZE
//                   && mask[indexLeft + (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            leftSideRow += inner;
//            isLeftGoingLeft = false;
//        }
//        else if(mask[indexLeft] != WHITE_PIXEL)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < 5)
//            {
//                inner++;
//                nextIndex = indexLeft + (inner * WIDTH);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = leftSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = leftSideRow;
//                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ",leftSideRow, leftSideRow);
//
//                fillShape(bg, dss, mask, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", 0, 0);
//
//                leftSideRow = lastLeftSideRow - 1;
//            }
//        }
//
//
//        if (leftSideRow < 0)
//            leftSideRow = 0;
//
////        __android_log_print(ANDROID_LOG_ERROR, "tag", "called top> first> %d    %d ", leftSideRow, rightSideRow);
//
//
//        ///////////////////right side II/////////////////////
//
//        inner = 0;
//        while (indexRight + (WIDTH * inner) < MAX_SIZE
//               && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//        {
//            inner++;
//        }
//
//        if (inner == 0)
//        {
//            distance = 0;
//            foundEdge = false;
//            inner = 0;
//            do
//            {
//                inner++;
//                nextIndex = indexRight + (WIDTH * inner);
//                if ( nextIndex < MAX_SIZE
//                     &&
//                     (mask[nextIndex] == WHITE_PIXEL
//                      || mask[nextIndex - 1] == WHITE_PIXEL
//                      || mask[nextIndex + 1] == WHITE_PIXEL
////                      || mask[nextIndex - 2] == WHITE_PIXEL
////                      || mask[nextIndex + 2] == WHITE_PIXEL
//                )
//                        )
//                {
//                    foundEdge = true;
//                    break;
//                }
//            } while (distance++ < MAX_DISTANCE); // same
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex + 2] == WHITE_PIXEL )
//                    {
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (mask[nextIndex + 3] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH] == WHITE_PIXEL
////                            ||mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                            || mask[nextIndex + WIDTH + 3] == WHITE_PIXEL)
////                                )
////                        if(nextIndex + WIDTH + 3 < MAX_SIZE &&
////                           (//mask[nextIndex + 3] == WHITE_PIXEL
//////                            ||
////                            (mask[nextIndex + WIDTH + 2] == WHITE_PIXEL
////                               && ( nextIndex + WIDTH + WIDTH + 3< MAX_DISTANCE
////                                    && mask[nextIndex + WIDTH + WIDTH + 3] == WHITE_PIXEL)
////                            )
////                            ||
////                            (//nextIndex + WIDTH + 3 < MAX_SIZE &&
////                             mask[nextIndex + WIDTH + 3] == WHITE_PIXEL
////                             && (// mask[nextIndex - WIDTH + 3] == WHITE_PIXEL ||
////                                     (nextIndex + WIDTH + WIDTH + 4 < MAX_SIZE
////                                      &&    mask[nextIndex + WIDTH + WIDTH + 4] == WHITE_PIXEL
////                                     //    && mask[nextIndex + WIDTH - WIDTH + 3] == WHITE_PIXEL
////                                     )
////                             )
////                            )
////                                   //|| mask[nextIndex - WIDTH + 3] == WHITE_PIXEL
////                           )
////                                )
////                            inner = 0;
//                        foundEdge = true;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // above +2
//            }
//
//            if(!foundEdge)
//            {
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight + (WIDTH * inner);
//                    if ( nextIndex < MAX_SIZE && mask[nextIndex - 2] == WHITE_PIXEL )
//                    {
//                        foundEdge = true;
//                        inner = 1;
//                        break;
//                    }
//                } while (inner < MAX_DISTANCE); // below -2
//            }
//
//            /////////////////////going to the other direction
//            if (!foundEdge)
//            {
//                distance = 0;
//                foundEdge = false;
//                inner = 0;
//                do
//                {
//                    inner++;
//                    nextIndex = indexRight - (WIDTH * inner);
//                    if ( nextIndex > 0
//                         &&
//                         (mask[nextIndex] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 1] == WHITE_PIXEL
//                          || mask[nextIndex + 1] == WHITE_PIXEL
//                          //                          || mask[nextIndex - 2] == WHITE_PIXEL
//                          || mask[nextIndex + 2] == WHITE_PIXEL
//                         )
//                            )
//                    {
//                        foundEdge = true;
//                        break;
//                    }
//                } while (distance++ < MAX_DISTANCE);
//
//                if (!foundEdge)
//                    inner = 0;
//                else inner *= -1;
//            }
//        }
//
//        ////////////// handling edges
//
//        if (inner > 0)
//        {
//            inner++;
//            while (indexRight + (WIDTH * inner) > 0
//                   && mask[indexRight + (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow += inner;
//            isRightGoingRight = true;
//        }
//        else if(inner < 0)
//        {
//            inner *= -1;
//            inner++;
//            while (indexRight - (WIDTH * inner) > 0
//                   && mask[indexRight - (WIDTH * inner)] == WHITE_PIXEL)
//            {
//                inner++;
//            }
//            inner--;
//            rightSideRow -= inner;
//            isRightGoingRight = false;
//        }
//        else if(mask[indexRight] != WHITE_PIXEL)
//        {
//            unsigned int selForTopCol = 0, selForTopRow = 0;
//            while(inner < 5)
//            {
//                inner++;
//                nextIndex = indexRight - (inner * WIDTH);
//                if(nextIndex + 3 < MAX_SIZE && mask[nextIndex + 3] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 3;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 4 < MAX_SIZE && mask[nextIndex + 4] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 4;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//                if(nextIndex + 5 < MAX_SIZE && mask[nextIndex + 5] == WHITE_PIXEL)
//                {
//                    selForTopCol = c + 5;
//                    selForTopRow = rightSideRow + inner;
//                    break;
//                }
//            }
//
//            if(selForTopCol != 0)
//            {
//                unsigned int resTopCol = 0, resTopRow = 0;
//
//                lastLeftSideRow = leftSideRow;
//                lastRightSideRow = rightSideRow;
//
//                getToTop(bg, mask, selForTopCol, selForTopRow, false, &resTopCol, &resTopRow);
//
//                fillShape(bg, dss, mask, resTopCol, resTopRow, c, &lastLeftSideRow, &lastRightSideRow);
//                rightSideRow = lastRightSideRow + 1;
//            }
//        }
//
//        if (rightSideRow > HEIGHT)
//            rightSideRow = HEIGHT;
//
//        ///////////////////process edges/////////////////////
//
//        if (leftSideRow < rightSideRow)
//        {
//            *lastLeftRow = leftSideRow;
//            *lastRightRow = rightSideRow;
//
//            index = ((leftSideRow * WIDTH) + c) * 4;
//            for (inner = leftSideRow; inner < rightSideRow; inner++, index += WIDTH_X_4)
//            {
//                bg[index] = dss[index];
//                bg[index + 1] = dss[index + 1];
//                bg[index + 2] = dss[index + 2];
//            }
//        }
//
//        c--;
//        columnLeft--;
//        columnRight--;
//    }
//
//}