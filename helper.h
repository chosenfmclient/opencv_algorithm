//
// Created by Ben on 27/09/2016.
//

#ifndef BLINGY_OCV_NDK_HELPER_H
#define BLINGY_OCV_NDK_HELPER_H

#include <android/log.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <stdlib.h>
#include <stdio.h>

using namespace cv;

class helper
{
    public:
        const static int WIDTH = 640, HEIGHT = 480, WIDTH_X_4 = WIDTH * 4, WIDTH_X_3 = WIDTH * 3
            , MAX_DISTANCE = 3, MAX_SIZE = WIDTH * HEIGHT, MAX_SIZE_X_4 = WIDTH * HEIGHT * 4, MAX_SIZE_X_3 = WIDTH * HEIGHT * 3
            , WHITE_PIXEL = 255, HUE_THRESHOLD = 1

            , DISTANCE_FROM_LINE = 8, DISTANCE_FROM_LINE_HALF = DISTANCE_FROM_LINE / 2
            , DISTANCE_FROM_LINE_HALF_X_4 = 4 * DISTANCE_FROM_LINE_HALF, DISTANCE_FROM_LINE_X_4 = DISTANCE_FROM_LINE * 4
            , DISTANCE_FROM_LINE_HALF_X_3 = 3 * DISTANCE_FROM_LINE_HALF, DISTANCE_FROM_LINE_X_3 = DISTANCE_FROM_LINE * 3;
        int count;
        helper();

        virtual ~helper();

        void fillEdges(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, unsigned int faceLeft, unsigned int faceTop);

    private:
        void getTopOfHead(uchar* bg, uchar* mask, unsigned int faceLeft, unsigned int faceTop, unsigned int *topColumn, unsigned int *topRow);

        void getFirstObject(uchar* bg, uchar* mask, unsigned int *topColumn, unsigned int *topRow);

        void getToTop(uchar* bg, uchar* mask, unsigned int column, unsigned int row, bool isGoingRight, unsigned int *topCol, unsigned int *topRow);

        void fillShape(uchar* bg, uchar* dss, uchar* mask, uchar* hsv, unsigned int topCol, unsigned int topRow, unsigned int lastCol, int *lastLeftRow, int *lastRightRow);

        bool isNotInRange(uchar arg1, uchar arg2);

//        bool isInRange(uchar hue1, )

};

#endif //BLINGY_OCV_NDK_HELPER_H