//
// Created by Ben on 25/09/2016.
//
#include "fm_bling_blingy_ProcessImage.h"
#include <android/log.h>
#include <jni.h>

#include <opencv2/core/core.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <stdlib.h>
#include <stdio.h>

#include "helper.h"

using namespace cv;

const int edgeThresh = 1, lowThreshold = 20, max_lowThreshold = 60;
const int ratio = 3, kernel_size = 3;
const Size size(kernel_size, kernel_size);
helper util;


JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_init(JNIEnv *env, jobject obj)
{
//    __android_log_print(ANDROID_LOG_ERROR, "tag", "hello from jni");
//
//    blur( src_gray, detected_edges, Size(3,3) );
}

JNIEXPORT jbyteArray JNICALL Java_fm_bling_blingy_ProcessImage_processImage
        (JNIEnv *env, jobject obj, jbyteArray inBuffer, jbyteArray outBuffer, jint width, jint height, jint type)
{
    int len = width * height * 4;
    char* buffer = new char[len];

    env->GetByteArrayRegion(inBuffer, 0, len, reinterpret_cast<jbyte*>(buffer));

//    dst.create(640, 480, CV_8UC4);
//    src.create(640, 480, CV_8UC4);

//    to rgba
//    Imgproc.cvtColor(mYuvFrameData, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);

//    Utils.bitmapToMat(cacheBitmap, mInputMat);


//    cvtColor(src, dst, COLOR_BGR2GRAY);
//    blur(dst, dst, Size(3, 3));
//    Canny(dst, dst, 25, 40);

//    env->ReleaseByteArrayElements()

    jbyteArray array = env->NewByteArray (len);
    env->SetByteArrayRegion(array, 0, len, reinterpret_cast<jbyte*>(buffer));

    return array;
}

JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName,
    jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
//    jint faceLeft, jint faceTop, jint faceRight, jint faceBottom)
{
    Mat &dss = *((Mat *) addr);

    Mat mask(dss.size(), 24);

    //load bg
    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
    env->ReleaseStringUTFChars(bgFileName, nativeString);
    cvtColor(bg, bg, COLOR_BGR2RGBA);


    ////test



    ////////////////////


    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    blur(mask, mask, size);//Size(kernel_size, kernel_size));
    blur(mask, mask, size);//Size(kernel_size, kernel_size));
    Canny(mask, mask, lowThreshold, max_lowThreshold, kernel_size);


    Mat rgbMat (dss.size(), dss.type());


    Mat hsv(dss.size(), dss.type());
    cvtColor(dss, hsv, COLOR_BGR2HSV);

    __android_log_print(ANDROID_LOG_ERROR, "tag", "simaaa333>>> %d   %d", hsv.channels(), hsv.size());

//    if(true)
    {

    }





    //draw canny on bg for testing
    int WIDTH = 640, HEIGHT = 480, index, index2;
    for (int r = 0; r < HEIGHT; r++)
    {
        index = WIDTH * r * 4;
        index2 = WIDTH * r;
        for (int c = 0; c < WIDTH; c++, index += 4, index2++)
        {
            if (mask.data[index2] == 255)
            {
                bg.data[index] = 255;
                bg.data[index + 1] = 255;
                bg.data[index + 2] = 255;
            }
            else
            {
                bg.data[index] = 100;
                bg.data[index + 1] = 100;
                bg.data[index + 2] = 100;
            }

            // we get the image rotated, and so the face detection is rotated as well

            //  faceRight -> top
            //  faceTop   -> left
            //  faceLeft  -> bottom
            //  faveBottom-> right

            if(faceLeft!=0)
            {
                if (c == faceRight && r >= faceTop && r <= faceBottom)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 255;
                }

                if (r == faceTop && c >= faceLeft && c <= faceRight)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 255;
                }

                if (c == faceLeft && r >= faceTop && r <= faceBottom)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 255;
                }

                if (r == faceBottom && c >= faceLeft && c <= faceRight)
                {
                    bg.data[index] = 0;
                    bg.data[index + 1] = 0;
                    bg.data[index + 2] = 255;
                }
            }
        }
    }


    util.fillEdges(bg.data, dss.data, mask.data, hsv.data, faceTop, faceRight); // top -> left , right -> top

    bg.copyTo(dss);
}






//    int y = 0, x, index, index2 = 1, startedCol, distance;
//    int rightSideRow, leftSideRow;
//    bool foundObject(false), foundEdge;
//
//    //draw canny on bg for testing
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    y=0;
//    for(int c=WIDTH-1, r;c > 1; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=WIDTH_X_4, index2+=WIDTH, x++)
//        {
//            if(foundObject)
//            {
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startedCol = c;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
//            int indexRight, indexLeft, inner;
//
//            while(c > 1)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                        && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    distance = 0;
//                    foundEdge = false;
//                    inner = 1;
//                    do{
//                        inner++;
//                        if ( indexLeft - (WIDTH * inner) > 0
//                                &&
//                                    (mask.data[indexLeft - (WIDTH * inner)] == 255
//                                    || mask.data[indexLeft - (WIDTH * inner) - 1] == 255
//                                    || mask.data[indexLeft - (WIDTH * inner) + 1] == 255
//                                    )
//                                )
//                        {
//                            foundEdge = true;
//                            break;
//                        }
//                    }while(distance++ < MAX_DISTANCE);
//
//                    if(!foundEdge)
//                        inner = 0;
//
//
////                    inner = 2;
////                    while (indexLeft - (WIDTH * inner) > 0
////                           &&
////                           mask.data[indexLeft - (WIDTH * inner)] == 255
////                            )
////                    {
////                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
////
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    distance = 0;
//                    foundEdge = false;
//                    inner = 1;
//                    do{
//                        inner++;
//                        if ( indexRight + (WIDTH * inner) > 0
//                             &&
//                             (mask.data[indexRight + (WIDTH * inner)] == 255
//                              || mask.data[indexRight + (WIDTH * inner) - 1] == 255
//                              || mask.data[indexRight + (WIDTH * inner) + 1] == 255
//                             )
//                                )
//                        {
//                            foundEdge = true;
//                            break;
//                        }
//                    }while(distance++ < MAX_DISTANCE);
//
//                    if(!foundEdge)
//                        inner = 0;
//
//
////                    inner = 2;
////                    while (indexLeft - (WIDTH * inner) > 0
////                           &&
////                           mask.data[indexLeft - (WIDTH * inner)] == 255
////                            )
////                    {
////                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
////
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
////                if(inner == 0)
////                {
////                    inner = 2;
////                    while (indexRight + (WIDTH * inner) > 0
////                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
////                        inner++;
////                    }
////                    if(inner == 2)
////                        inner = 0;
////                    else inner--;
////                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                ///////////////////process edges/////////////////////
//                index = ((leftSideRow * WIDTH) + c) * 4;
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=WIDTH_X_4 )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//    bg.copyTo(dss);
//}













//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName)
//{
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    //draw canny on bg for testing
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    int rightSideRow, leftSideRow;
//    bool foundObject = false;
//    y=0;
//    for(int c=WIDTH, r;c > 0; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=(WIDTH*4), index2+=WIDTH, x++)
//        {
////            if(x < 400 && y < 200)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
////                mask.data[index2]=255;
////            }
//
////            if(index < (WIDTH*4) * 10.5f)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
//////                mask.data[index2]=255;
////                continue;
////            }
////            else break;
//            if(foundObject)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startCol = column;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
//            int indexRight, indexLeft, inner;
//
//            while(c > 0)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                        && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//
//                    while (indexLeft - (WIDTH * inner) > 0
//                           &&
//                           mask.data[indexLeft - (WIDTH * inner)] == 255
//
//                            )
//                    {
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
//
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//                    while (indexRight + (WIDTH * inner) > 0
//                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
//                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                ///////////////////process edges/////////////////////
//                index = ((leftSideRow * WIDTH) + c) * 4;
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=(WIDTH*4) )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//    bg.copyTo(dss);
//}



//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr(JNIEnv *env, jobject obj, jlong addr, jstring bgFileName)
//{
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    //draw canny on bg
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//
//
//
//        }
//    }
//
//    int rightSideRow, leftSideRow;
//    bool foundObject = false;
//    y=0;
//    for(int c=WIDTH, r;c > 0; c--, y++)
//    {
////        index = (WIDTH * HEIGHT * 4) - 4 - (c * 4);
//        index = (c * 4) - 4;
//        index2 = c - 1;
//        x = 0;
//        for(r=1;r<HEIGHT;r++, index+=(WIDTH*4), index2+=WIDTH, x++)
//        {
////            if(x < 400 && y < 200)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
////                mask.data[index2]=255;
////            }
//
////            if(index < (WIDTH*4) * 10.5f)
////            {
////                dss.data[index + 2]=255;
////                dss.data[index + 1]=0;
////                dss.data[index]=0;
//////                mask.data[index2]=255;
////                continue;
////            }
////            else break;
//            if(foundObject)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    rightSideRow = r;
//                    break;
//                }
//            }
//
//            if(mask.data[index2] == 255)
//            {
//                foundObject = true;
//
//                startCol = column;
//                rightSideRow = leftSideRow = r;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//            bg.data[index] = 255;
//            bg.data[index + 1] = 0;
//            bg.data[index + 2] = 0;
//        }
//
//        if(foundObject)
//        {
//            rightSideRow = r;
////            if(endRow > HEIGHT)
////                endRow = HEIGHT;
//            int indexRight, indexLeft, inner;
//
//
//            __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>>> %d  %d  %d", leftSideRow, rightSideRow, c);
//
//            while(c > 0)// && column < WIDTH - distance - 2)
//            {
//                c--;
//                indexRight = (rightSideRow * WIDTH) + c;
//                indexLeft = (leftSideRow * WIDTH) + c;
//
//                ///////////////////left side/////////////////////
//                inner = 1;
//                while ( indexLeft - (WIDTH * inner) > 0
//                       && mask.data[indexLeft - (WIDTH * inner)] == 255 )
//                {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//
//                    while (indexLeft - (WIDTH * inner) > 0
//                           &&
//                            mask.data[indexLeft - (WIDTH * inner)] == 255
//
//                            )
//                    {
//                        __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>2>> %d  %d  %d", indexLeft, (WIDTH * inner), c);
//
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
////                    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>1>> %d  %d  %d  %d  %d", indexLeft, (WIDTH * inner), c, leftSideRow, inner);
//
////                    inner--;
////                    if(inner == 1)
////                        inner == 0;
//                }
//
//
//                if (inner > 0) {
//                    leftSideRow -= inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexLeft + (WIDTH * inner) > 0
//                           && mask.data[indexLeft + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    leftSideRow += inner;
//                }
//
//                if(leftSideRow < 0)
//                    leftSideRow = 0;
//
//                ///////////////////right side/////////////////////
//                inner = 1;
//                while (indexRight + (WIDTH * inner) > 0
//                       && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                    inner++;
//                }
//                inner--;
//
//                if(inner == 0)
//                {
//                    inner = 2;
//                    while (indexRight + (WIDTH * inner) > 0
//                           && mask.data[indexRight + (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    if(inner == 2)
//                        inner = 0;
//                    else inner--;
//                }
//
//                if (inner > 0) {
//                    rightSideRow += inner;
//                }
//                else {
//                    inner = 1;
//                    while (indexRight - (WIDTH * inner) < WIDTH * HEIGHT
//                           && mask.data[indexRight - (WIDTH * inner)] == 255) {
//                        inner++;
//                    }
//                    inner--;
//                    rightSideRow -= inner;
//                }
//
//                if(rightSideRow > HEIGHT)
//                    rightSideRow = HEIGHT;
//
//                index = ((leftSideRow * WIDTH) + c) * 4;
////
//                for(inner = leftSideRow; inner < rightSideRow; inner++, index+=(WIDTH*4) )
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//
//            }
//
//            break;
//        }
//    }
//
//    bg.copyTo(dss);
//
//    if(true)
//        return;
//
////    if(nextLeftRow < 1)
////        nextLeftRow = 1;
////
////    index = ((nextLeftRow * WIDTH) + column) * 4;
////
////    for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
////    {
////
//////                index2 = (inner * WIDTH) + column;
////
////        bg.data[index] = dss.data[index];
////        bg.data[index + 1] = dss.data[index + 1];
////        bg.data[index + 2] = dss.data[index + 2];
////
////    }
//
//
//    //    bg.copyTo(dss);
//
//
//
////    for(int i=0; i<)
//
//    for (;//int column = WIDTH, row, y = 0, x, index, index2 = 1;
//            column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//
////    for (column = 0; col//int column = WIDTH, row, y = 0, x, index, index2 = 1;
////            column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 = (WIDTH * HEIGHT) - 1 - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2-= WIDTH)//-= column)
//        {
////            if(true)
////            {
////                bg.data[index] = dss.data[index];
////                bg.data[index + 1] = dss.data[index + 1];
////                bg.data[index + 2] = dss.data[index + 2];
////                continue;
////            }
//            if(x<250 && y < 100)
//            {
//                dss.data[index] = 0;//dss.data[index];
//                dss.data[index + 1] = 0;//dss.data[index + 1];
//                dss.data[index + 2] = 255;
//
//                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev43>>> %d  %d  %d", index, row, column);
//
//                continue;
//            }
//            else continue;
////            if(row < 140 && column < 40)
////            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 255;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////            }
//
//            if(found)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    endRow = row;
//                    break;
//                }
//            }
//            if(mask.data[index2] == 255)
//            {
//                helper help;
//
//                try
//                {
//                    help.drawRecAroundPoint(bg.data, index, WIDTH);
//                }
//                catch (...)
//                {
//
//                }
//
//                found = true;
////                break;
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
//                startCol = column;
//                endRow = startRow = row;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//        }
//        if(found)
//            break;
//    }
//
//
//
//    if(found)
//    {
//        int nextLeftRow = startRow, nextRightRow = endRow;
////        int startRow = row;
//        int indexRight, indexLeft, inner;
//        int i;
//
//        const int distance = 2;
//
//        while(found && column > distance + 2)// && column < WIDTH - distance - 2)
//        {
//            column--;
//
////            index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
////            index2 = (WIDTH * HEIGHT) - 1 - column;
//
//
//
//            indexRight = (nextRightRow * WIDTH) + column;
//            indexLeft = (nextLeftRow * WIDTH) + column;
//
////            if(indexLeft < distance + 2) break;
////            if(indexRight < distance + 2) break;
////            if(indexRight > WIDTH - distance - 2) break;
////            if(indexLeft > WIDTH - distance - 2) break;
//
////            if(indexLeft > ((HEIGHT*WIDTH) + column)- (distance*WIDTH) )
////            {
////                nextLeftRow = HEIGHT;
////            }
////            else {
//
//            bool firstMiss;
//            i = 1;
//            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
//                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
//                i++;
//            }
//
////            i++;
////            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
////                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//            if (i > 0) {
//                nextLeftRow -= i;
//            }
////                else {
////                    i = 1;
////                    while (indexLeft - (WIDTH * i) > 0
////                           && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow += i;
////                }
//
//
//            i = 1;
//            while (indexRight - (WIDTH * i) > 0
//                   && mask.data[indexRight - (WIDTH * i)] == 255) {
//                i++;
//            }
////            i++;
////            while (indexRight - (WIDTH * i) > 0
////                   && mask.data[indexRight - (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//
//            if (i > 0) {
//                nextRightRow += i;
//            }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
//
//
////            }
////
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) > 0
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow -= i;
////                }
////
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexRight - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
////            }
//
//
//
////                do {
////                    try {
////
////
////                        if (mask.data[indexLeft + (WIDTH * i)] == 255) // 4
////                        {
////                            nextLeftRow -= i;
////                            break;
////                        }
////                    }
////                    catch (...) {
////                        nextLeftRow = HEIGHT;
////                        break;
////                    }
////
////                } while (i-- > 1);
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexLeft - (WIDTH * i)] == 255) // 4
////                            {
////                                nextLeftRow += i;
////                                break;
////                            }
////                        }
////                        catch (...) {
////                            nextLeftRow = 0;
////                            break;
////                        }
////
////                    } while (i-- > 1);
////                }
////            }
////            if(mask.data[indexLeft + WIDTH] == 255) // 4
////            {
////                nextLeftRow--;
////            }
////            else if(mask.data[indexLeft - WIDTH] == 255) // 6
////            {
////                nextLeftRow++;
////            }
//
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////                i = distance;
////                do {
////                    try {
////                        if (mask.data[indexRight - (WIDTH * i)] == 255) // 4
////                        {
////                            nextRightRow -= i;
////                            break;
////                        }
////                    } catch (...) {
////                        nextRightRow = 0;
////                        break;
////                    }
////                } while (i-- > 1);
////
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexRight + (WIDTH * i)] == 255) // 4
////                            {
////                                nextRightRow += i;
////                                break;
////                            }
////                        } catch (...) {
////                            nextRightRow = HEIGHT;
////                            break;
////                        }
////                    } while (i-- > 1);
////                }
////            }
//
//
//
////            if(mask.data[indexRight - WIDTH] == 255) // 4
////            {
////                nextRightRow--;
////            }
////            else if(mask.data[indexRight + WIDTH] == 255) // 6
////            {
////                nextRightRow++;
////            }
//
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev23>>> %d  %d", nextLeftRow, nextRightRow);//,//dss.cols,
//
//            if(nextLeftRow < 1)
//                nextLeftRow = 1;
//
//            index = ((nextLeftRow * WIDTH) + column) * 4;
//
//            for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
//            {
//
////                index2 = (inner * WIDTH) + column;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//
////            bg.data[nextLeftRow] = 255;
////            bg.data[nextLeftRow + 1] = 255;
////            bg.data[nextLeftRow + 2] = 255;
////
////            bg.data[nextRightRow] = 255;
////            bg.data[nextRightRow + 1] = 255;
////            bg.data[nextRightRow + 2] = 255;
//
//        }
//    }
//
////    bg.copyTo(dss);
//
//}






















//JNIEXPORT void JNICALL Java_fm_bling_blingy_ProcessImage_processImageAddr
//        (JNIEnv *env, jobject obj, jlong addr, jstring bgFileName) {
//    Mat &dss = *((Mat *) addr);
//    Mat mask(dss.size(), 24);
//
//
//    //load bg
//    const char *nativeString = env->GetStringUTFChars(bgFileName, JNI_FALSE);
//    Mat bg = imread(nativeString, -10);//CV_LOAD_IMAGE_COLOR);// >0
//    env->ReleaseStringUTFChars(bgFileName, nativeString);
//
//    cvtColor(bg, bg, COLOR_BGR2RGBA);
//
//
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", bg.type(), bg.rows, bg.cols);
////    if(true)
////    {
////        bg.copyTo(dss);
////        return;
////    }
//
//    cvtColor(dss, mask, COLOR_RGBA2GRAY);
//    blur(mask, mask, Size(3, 3));
//    Canny(mask, mask, 10, 60);
//
////    __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev>>> %d  %d  %d   %d", dss.type(), dss.cols,
////                        mask.type(), mask.cols);
//
//
//    int WIDTH = mask.cols, HEIGHT = mask.rows;
//
//
//    int column = WIDTH, row, y = 0, x, index, index2 = 1;
//    bool found(false);
//    int startCol, startRow, endRow;
//
//    for(int r=0;r<HEIGHT;r++)
//    {
//        index = WIDTH * r * 4;
//        index2 = WIDTH * r;
//        for(int c=0;c<WIDTH;c++, index+=4, index2++)
//        {
//            if(mask.data[index2]==255)
//            {
//                bg.data[index] = 255;
//                bg.data[index + 1] = 255;
//                bg.data[index + 2] = 255;
//            }
//        }
//    }
//
//    for (;//int column = WIDTH, row, y = 0, x, index, index2 = 1;
//         column >= 0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 = (WIDTH * HEIGHT) - 1 - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2-= WIDTH)//-= column)
//        {
////            if(true)
////            {
////                bg.data[index] = dss.data[index];
////                bg.data[index + 1] = dss.data[index + 1];
////                bg.data[index + 2] = dss.data[index + 2];
////                continue;
////            }
////            if(x<200 && y < 200)
////            {
////                bg.data[index] = 0;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////                continue;
////            }
////            else break;
////            if(row < 140 && column < 40)
////            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 255;//dss.data[index + 1];
////                bg.data[index + 2] = 255;
////            }
//
//            if(found)
//            {
////                bg.data[index] = 255;//dss.data[index];
////                bg.data[index + 1] = 0;//dss.data[index + 1];
////                bg.data[index + 2] = 0;
//
//                if(mask.data[index2] == 255)
//                {
//                    bg.data[index] = dss.data[index];
//                    bg.data[index + 1] = dss.data[index + 1];
//                    bg.data[index + 2] = dss.data[index + 2];
//                }
//                else
//                {
//                    endRow = row;
//                    break;
//                }
//            }
//            if(mask.data[index2] == 255)
//            {
//                helper help;
//
//                try
//                {
//                    help.drawRecAroundPoint(bg.data, index, WIDTH);
//                }
//                catch (...)
//                {
//
//                }
//
//                found = true;
////                break;
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
//                startCol = column;
//                endRow = startRow = row;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//        }
//        if(found)
//            break;
//    }
//
//
//
//    if(found)
//    {
//        int nextLeftRow = startRow, nextRightRow = endRow;
////        int startRow = row;
//        int indexRight, indexLeft, inner;
//        int i;
//
//        const int distance = 2;
//
//        while(found && column > distance + 2)// && column < WIDTH - distance - 2)
//        {
//            column--;
//
////            index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
////            index2 = (WIDTH * HEIGHT) - 1 - column;
//
//
//
//            indexRight = (nextRightRow * WIDTH) + column;
//            indexLeft = (nextLeftRow * WIDTH) + column;
//
////            if(indexLeft < distance + 2) break;
////            if(indexRight < distance + 2) break;
////            if(indexRight > WIDTH - distance - 2) break;
////            if(indexLeft > WIDTH - distance - 2) break;
//
////            if(indexLeft > ((HEIGHT*WIDTH) + column)- (distance*WIDTH) )
////            {
////                nextLeftRow = HEIGHT;
////            }
////            else {
//
//                bool firstMiss;
//                i = 1;
//                while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
//                       && mask.data[indexLeft + (WIDTH * i)] == 255) {
//                    i++;
//                }
//
////            i++;
////            while (indexLeft + (WIDTH * i) < WIDTH * HEIGHT
////                   && mask.data[indexLeft + (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//                if (i > 0) {
//                    nextLeftRow -= i;
//                }
////                else {
////                    i = 1;
////                    while (indexLeft - (WIDTH * i) > 0
////                           && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow += i;
////                }
//
//
//                i = 1;
//                while (indexRight - (WIDTH * i) > 0
//                       && mask.data[indexRight - (WIDTH * i)] == 255) {
//                    i++;
//                }
////            i++;
////            while (indexRight - (WIDTH * i) > 0
////                   && mask.data[indexRight - (WIDTH * i)] == 255) {
////                i++;
////            }
////            i--;
//            i--;
//
//                if (i > 0) {
//                    nextRightRow += i;
//                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
//
//
////            }
////
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexLeft - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) > 0
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextLeftRow -= i;
////                }
////
////
////                i = 1;
////                while (indexRight - (WIDTH * i) > 0
////                       && mask.data[indexRight - (WIDTH * i)] == 255) {
////                    i++;
////                }
////                i--;
////
////                if (i > 0) {
////                    nextRightRow += i;
////                }
////                else {
////                    i = 1;
////                    while (indexRight + (WIDTH * i) < WIDTH * HEIGHT
////                           && mask.data[indexRight + (WIDTH * i)] == 255) {
////                        i++;
////                    }
////                    i--;
////                    nextRightRow -= i;
////                }
////            }
//
//
//
////                do {
////                    try {
////
////
////                        if (mask.data[indexLeft + (WIDTH * i)] == 255) // 4
////                        {
////                            nextLeftRow -= i;
////                            break;
////                        }
////                    }
////                    catch (...) {
////                        nextLeftRow = HEIGHT;
////                        break;
////                    }
////
////                } while (i-- > 1);
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexLeft - (WIDTH * i)] == 255) // 4
////                            {
////                                nextLeftRow += i;
////                                break;
////                            }
////                        }
////                        catch (...) {
////                            nextLeftRow = 0;
////                            break;
////                        }
////
////                    } while (i-- > 1);
////                }
////            }
////            if(mask.data[indexLeft + WIDTH] == 255) // 4
////            {
////                nextLeftRow--;
////            }
////            else if(mask.data[indexLeft - WIDTH] == 255) // 6
////            {
////                nextLeftRow++;
////            }
//
////            if(indexRight < WIDTH*distance)
////            {
////                nextRightRow = 0;
////            }
////            else
////            {
////                i = distance;
////                do {
////                    try {
////                        if (mask.data[indexRight - (WIDTH * i)] == 255) // 4
////                        {
////                            nextRightRow -= i;
////                            break;
////                        }
////                    } catch (...) {
////                        nextRightRow = 0;
////                        break;
////                    }
////                } while (i-- > 1);
////
////                if(i<=1)
////                {
////                    i = distance;
////                    do {
////                        try {
////                            if (mask.data[indexRight + (WIDTH * i)] == 255) // 4
////                            {
////                                nextRightRow += i;
////                                break;
////                            }
////                        } catch (...) {
////                            nextRightRow = HEIGHT;
////                            break;
////                        }
////                    } while (i-- > 1);
////                }
////            }
//
//
//
////            if(mask.data[indexRight - WIDTH] == 255) // 4
////            {
////                nextRightRow--;
////            }
////            else if(mask.data[indexRight + WIDTH] == 255) // 6
////            {
////                nextRightRow++;
////            }
//
//
////                __android_log_print(ANDROID_LOG_ERROR, "tag", "SimLev23>>> %d  %d", nextLeftRow, nextRightRow);//,//dss.cols,
//
//            if(nextLeftRow < 1)
//                nextLeftRow = 1;
//
//            index = ((nextLeftRow * WIDTH) + column) * 4;
//
//            for(inner = nextLeftRow; inner < nextRightRow; inner++, index-=(WIDTH*4) )
//            {
//
////                index2 = (inner * WIDTH) + column;
//
//                bg.data[index] = dss.data[index];
//                bg.data[index + 1] = dss.data[index + 1];
//                bg.data[index + 2] = dss.data[index + 2];
//            }
//
////            bg.data[nextLeftRow] = 255;
////            bg.data[nextLeftRow + 1] = 255;
////            bg.data[nextLeftRow + 2] = 255;
////
////            bg.data[nextRightRow] = 255;
////            bg.data[nextRightRow + 1] = 255;
////            bg.data[nextRightRow + 2] = 255;
//
//        }
//    }
//
//    bg.copyTo(dss);
//
//}





//    for(int row=0, index; row<480;row++)
//    {
//        index = (row * 640) * 4;
//        for(int col = 0; col<640;col++, index+=4)
//        {
//
////            dss.data[index] = 0;
////            dss.data[index + 1] = 0;
////            dss.data[index + 2] = 255;
//
//            if(mask.data[index + 1] == 255)
//            {
//                mask.data[index + 1] = 0;
//                mask.data[index + 2] = 255;
//            }
//
////            __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", mask.data[index], mask.data[index + 1], mask.data[index + 2]);
//
//        }
//    }



//    for (int row = 0, index = 0; row < HEIGHT; row++) {
//        index = row * WIDTH;
//        for (int col = 0; col < WIDTH; col++, index++) {
//            if (col < 240 && row < 100) {
//                mask.data[index] = 255;
//            }
//        }
//    }

//    for (int row = 0, index = 0; row < HEIGHT; row++) {
//        index = row * WIDTH * 4;
//        for (int col = 0; col < WIDTH; col++, index += 4) {
//            if (col > 440 && row > 400) {
//                dss.data[index] = 255;
//                dss.data[index + 1] = 255;
//                dss.data[index + 2] = 255;
//            }
//        }
//    }




//    mask.copyTo(dss);


//    for(int column = WIDTH, row, index; column>0; column--)
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//    }

//    for(int column = WIDTH, row, y=0, x, index, index2 = 1; column>=0; column--, y++) // index = ((row * WIDTH) + column) * 4
//    {
//        index = (WIDTH * HEIGHT * 4) - 4 - (column * 4);
//        index2 -= 1;//(WIDTH * HEIGHT) - column;
//
//        for (row = HEIGHT - 1, x = 0; row > 0; row -= 1, index -= (WIDTH * 4), x++, index2++)//-= column)
//        {
//
//            if(index < 0 || index + 3 > ( WIDTH * HEIGHT * 4))
//            {
//                __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d   %d", index, ( WIDTH * HEIGHT * 4), row, column);
////                __android_log_print(ANDROID_LOG_ERROR, "tag","SimLev>>> %d  %d  %d", mask.data[index], mask.data[index + 1], mask.data[index + 2]);
//                return;
//            }
//            if(column< 20 && row < 20)
//                mask.data[index2]=255;
////            if(mask.data[index2] == 255)
////            {
////                dss.data[index] = 255;
////                dss.data[index + 1] = 255;
////                dss.data[index + 2] = 255;
////            }
//
//
////            if(mask.data[index + 1] == 255)
////            {
////                mask.data[index + 1] = 0;
////                mask.data[index + 2] = 255;
////            }
//        }
//
//    }

//  for(int row=0, index; row<640;row++)
//    {
//        index = (row * 480) * 4;
//        for(int col = 0; col<480;col++, index+=4)
//        {
//            int temp = bg.data[index];
//            bg.data[index] = bg.data[index + 2];
////            bg.data[index + 1] = 0;
//            bg.data[index + 2] = temp;
//
//
//        }
//    }