//
//  faceDetector.cpp
//  BlingIphoneApp
//
//  Created by Michael Biehl on 1/22/17.
//  Copyright © 2017 Singr FM. All rights reserved.
//

#include "faceDetector.h"

using namespace cv;

float rot = 0.0;
int detectionFailCount = 0;
CascadeClassifier faceCascade;
CascadeClassifier eyeCascade;

std::vector<cv::Rect> lastFaceAndEyes = {cv::Rect(), cv::Rect(), cv::Rect()};
cv::Rect lastFace;

FaceDetector::FaceDetector (String faceCascadePath, String eyeCascadePath) {
    faceCascade.load(faceCascadePath);
    eyeCascade.load(eyeCascadePath);
}

FaceDetector::~FaceDetector() { };

void FaceDetector::getFaceRect(const Mat& image) {
    std::vector<cv::Rect> faces;
    cv::Size originalSize = image.size();
    Mat greyImg;
    cvtColor(image, greyImg, CV_RGBA2GRAY);
    resize(greyImg, greyImg, cv::Size(), 0.5f, 0.5f);
    equalizeHist(greyImg, greyImg);
    
    if (rot != 0) {
        cv::Point center(float(greyImg.cols) / 2 , float(greyImg.rows)/2);
        Mat rotMat = getRotationMatrix2D(center, rot, 1);
        warpAffine(greyImg, greyImg, rotMat, greyImg.size());
    }
    
    faceCascade.detectMultiScale( greyImg, faces, 1.35, 3, 0, cv::Size(greyImg.size().height * 0.075,
                                                                       greyImg.size().height * 0.075));
    
    if (!faces.size())
    {
        if (detectionFailCount > 5) {
            rot += detectionFailCount % 2 == 0 ? detectionFailCount * 1.05 : detectionFailCount * -1.05;
        }
        
        if (++detectionFailCount > 20) {
            rot = 0.0;
            detectionFailCount = 0;
        }
    }
    else
    {
        detectionFailCount = 0;
        Mat faceImg = greyImg(faces[0]);
        
        if (faceImg.data == NULL) return;
        
        std::vector<cv::Rect> eyes;
        resize(faceImg, faceImg, cv::Size(100,100));
        equalizeHist(faceImg, faceImg);
        cv::Size eyeMin(faceImg.size().width * 0.18, faceImg.size().width * 0.18);
        cv::Size eyeMax(faceImg.size().width * 0.28, faceImg.size().width * 0.28);
        eyeCascade.detectMultiScale(faceImg, eyes, 1.2, 3, 0, eyeMin, eyeMax);//, eyeMin, eyeMax);
        if (eyes.size())
        {
            lastFaceAndEyes[0] = faces[0];
            lastFaceAndEyes[1] = eyes[0] + faces[0].tl(); // offset eyes to be inside face rect
            lastFaceAndEyes[2] = eyes[1] + faces[0].tl();
            cv::Rect firstEye = eyes[0];
            cv::Rect secEye = eyes[1];
            if ((firstEye & secEye) != cv::Rect()) // if the eye rects intersect, r1 & r2 = intersection, don't use it!
            {
                return;
            }
            float leftX = MIN(firstEye.x, secEye.x);
            float rightX = MAX(firstEye.x, secEye.x);
            cv::Rect leftEye = leftX == firstEye.x ? firstEye : secEye;
            cv::Rect rightEye = rightX == firstEye.x ? firstEye : secEye;
            float newAngle = atanf(float((leftEye.tl().y - rightEye.tl().y)) / float((leftEye.tl().x - rightEye.tl().x))) * (180.0 / 3.1415927); // rads to degs :)
            rot += newAngle < 20 ? newAngle : 0;
        }
        cv::Rect face = lastFaceAndEyes[0];
        face += cv::Size(face.size().width, face.size().height); // transform face rect back
        face += cv::Point(face.tl().x,
                          face.tl().y);

        cv::RotatedRect rotFace(Point2f(face.x + (face.size().width) / 2.0,
                                        face.y + (face.size().height) / 2.0),
                                face.size(),
                                rot);
        lastFace = rotFace.boundingRect() & cv::Rect(cv::Point(), originalSize); // take intersection with image so the face rect doesn't go beyond bounds
    }
}
